## Calendar UI

![Calendar UI](https://bitbucket.org/cleverua/custom-cell-calendar/downloads/calendar-ui.png)

### Colors

Calendar colors should be configurable througn theme 
Default theme color values:

1. Selected Day #dcded0
1. Not selected Day of Visible Month #eeeeee
1. Non-Visible Months Days #f8f8f8
1. Grid Lines #dcdee0
1. Day Number & Week Day Titles #a6aaa9
1. Week Day Line. It has a Background (drawable, can be transparent)

### Gestures

Calendar should respond for a vertical scroll event, it should scroll to previuos/next month

### Scrolling Area

Item 7 shows a scrollable area of calendar. (The Week Day Line put over the calendar days, grid lines)

### Callbacks

**onVisibleMonthChanged** --- fires when user scrolls to next/previous month

**onSelectedDateChanged** --- fires when user selects a day on Visible month (days in Non-Visible months are disabled and can't be selected)

### Instance Methods

**setCellOverlayFactory**(**CellOverlayFactory** cellOverlayFactory) --- factory instantiates an overlay View that implements CellOverlay interface

**setSelectedDate**(Date date) --- selects a day in calendar

**setVisibleMonth**(Date date) --- shows month of given date

**setAdapter**(**CalendarColorsAdapter** adapter) --- sets adapter that returns view of cell

**setWorkDaysOnly(boolean) --- show a full week (7 days) or workdays only (Mon, Tue, Wed, Thu, Fri). First day of week (for 7days) should be based on current device locale

### CalendarColorsAdapter 

CalendarColorsAdapter is a "dataset" implements interface declared

```
#!java
interface CalendarColorsAdapter {
  android.graphics.Color[] getColorsArray(Date date);
}
```

Calendar calls getColorsArray for each day. Recieved colors array (or empty array) it transmits to day's CellOverlay. Cell overlay do its own logic to draw given color array.

### CellOverlayFactory

Factory that instantiates a View implements CellOverlay. Each CellOverlay implementations show given colors in different way (like a bars or dots etc.)

```
#!java
interface CellOverlay {
  void setColorsArray(android.graphics.Color[]);
}
```

There is an example how CellOverlay could looks like:

![Overlay Cell 1](https://bitbucket.org/cleverua/custom-cell-calendar/downloads/overlay-cell-1.png)

![Overlay Cell 2](https://bitbucket.org/cleverua/custom-cell-calendar/downloads/overlay-cell-2.png)