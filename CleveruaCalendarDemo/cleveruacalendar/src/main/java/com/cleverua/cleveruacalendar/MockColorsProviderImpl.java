package com.cleverua.cleveruacalendar;

import android.graphics.Color;
import android.support.v4.util.ArrayMap;

import java.util.Date;
import java.util.Map;

/**
 * @author Artemiy Lysenko, on 9/23/15.
 */
public class MockColorsProviderImpl implements ColorsProvider {

    private Map<Date, Integer[]> source;

    public MockColorsProviderImpl() {
        source = new ArrayMap<>();
        long now = System.currentTimeMillis();
        long tomorrow = now + 24 * 60 * 60 * 1000L;
        long dayAfterTomorrow = tomorrow + 24 * 60 * 60 * 1000L;
        source.put(new Date(now), new Integer[]{Color.RED});
        source.put(new Date(tomorrow), new Integer[]{Color.CYAN});
        source.put(new Date(dayAfterTomorrow), new Integer[]{Color.BLUE});
    }

    @Override
    public Integer[] getColorsArray(Date date) {
        return source.get(date);
    }
}
