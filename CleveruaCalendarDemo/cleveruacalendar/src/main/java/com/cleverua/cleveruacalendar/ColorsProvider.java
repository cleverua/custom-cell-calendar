package com.cleverua.cleveruacalendar;

import java.util.Date;

/**
 * @author Artemiy Lysenko, on 9/22/15.
 */
public interface ColorsProvider {

    Integer[] getColorsArray(Date date);
}
