package com.cleverua.cleveruacalendar;

import android.view.View;

import java.util.Date;

/**
 * @author Artemiy Lysenko, on 9/25/15.
 */
public interface CalendarListener {

    void onVisibleMonthChanged(int previousMonth, int newMonth);

    void onSelectedDateChanged(Date selectedDate);

    void onSelectedDateChanged(View selectedCell);
}
