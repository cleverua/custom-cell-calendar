package com.cleverua.cleveruacalendar;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static android.view.ViewGroup.LayoutParams.*;

/**
 * @author Artemiy Lysenko, on 9/22/15.
 */
public class CleverUaCalendarView extends RelativeLayout implements View.OnTouchListener, AdapterView.OnItemClickListener {

    private static final String TAG = "CleverUaCalendarView";
    private static final String nameSpace = "http://schemas.android.com/apk/res-auto";
    private static final int MONTH_YEAR_FLAG = DateUtils.FORMAT_SHOW_DATE
                                                       | DateUtils.FORMAT_NO_MONTH_DAY
                                                       | DateUtils.FORMAT_SHOW_YEAR;
    private static String packageName;
    private CalendarListener listener;
    private Context context;
    private Locale defaultLocale;
    private Calendar calendar;
    private TextView monthYearTxtView;
    private boolean isSevenDaysWeek;
    ArrayAdapter<Integer> mockAdapter;


    private static final class Attributes {
        static final String IS_SEVEN_DAYS_WEEK = "isSevenDaysWeek";
    }

    private static final class Id {
        static final String id = "id";
        static final String TXT_VIEW_MONTH_YEAR = "txt_month_year";
        static final String LAYOUT_HEADER_DAYS = "layout_header_days";
        static final String HEADLINE = "headline";
    }

    private static final class Empty {
        static final String STRING = "";
        static final String[] STRING_ARRAY = new String[0];
    }

    public CleverUaCalendarView(Context context, boolean isSevenDaysWeek) {
        super(context);
        this.context = context;
        this.isSevenDaysWeek = isSevenDaysWeek;
        init(null);
    }

    public CleverUaCalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        isSevenDaysWeek = attrs.getAttributeBooleanValue(nameSpace, Attributes.IS_SEVEN_DAYS_WEEK, true);
        init(attrs);
    }

    public void setListener(CalendarListener listener) {
        this.listener = listener;
    }

    private void init(@Nullable AttributeSet attrs) {
        packageName = context.getPackageName();
        defaultLocale = Locale.getDefault();
        calendar = Calendar.getInstance(defaultLocale);

        //TODO: remove the mock listener
        listener = new MockCalendarListenerImpl();

        initMonthAndYearView();
        initDayHeaderViews(attrs);
        initHeadLine();
        initGridView();
    }

    private void initMonthAndYearView() {
        monthYearTxtView = new TextView(context);
        monthYearTxtView.setId(getResources().getIdentifier(Id.TXT_VIEW_MONTH_YEAR, Id.id, packageName));
        LayoutParams params = new LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        monthYearTxtView.setLayoutParams(params);
        monthYearTxtView.setAllCaps(true);
        monthYearTxtView.setFreezesText(true);
        monthYearTxtView.setTextColor(getResources().getColor(R.color.text_main));

        //TODO: set large font size

        String monthAndYear = getMonthAndYear();
        monthYearTxtView.setText(monthAndYear);
        addView(monthYearTxtView);
    }

    private String getMonthAndYear() {
        long now = calendar.getTimeInMillis();
        return DateUtils.formatDateRange(null, now, now, MONTH_YEAR_FLAG);
    }

    private void initDayHeaderViews(@Nullable AttributeSet attrs) {
        LinearLayout layoutHeaderDays = new LinearLayout(context);
        layoutHeaderDays.setId(getResources().getIdentifier(Id.LAYOUT_HEADER_DAYS, Id.id, packageName));
        layoutHeaderDays.setOrientation(LinearLayout.HORIZONTAL);
        LayoutParams params = new LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, monthYearTxtView.getId());
        layoutHeaderDays.setLayoutParams(params);

        TextView[] dayHeaderViews = isSevenDaysWeek ? new TextView[7] : new TextView[5];
        TableLayout.LayoutParams dayParams = new TableLayout.LayoutParams(0, WRAP_CONTENT, 1F);
        String[] dayNames = getDayNames();
        TextView dayNameView;
        final int daysCount = dayHeaderViews.length;
        int textColor = getResources().getColor(R.color.text_main);
        for (int i = 0; i < daysCount; i++) {
            dayNameView = new TextView(context);
            dayNameView.setLayoutParams(dayParams);
            dayNameView.setGravity(Gravity.CENTER);
            dayNameView.setTextColor(textColor);
            dayNameView.setText(dayNames[i]);
            layoutHeaderDays.addView(dayNameView);
        }
        addView(layoutHeaderDays);
    }

    private String[] getDayNames() {
        DateFormatSymbols dateFormatSymbols = DateFormatSymbols.getInstance(defaultLocale);
        List<String> days = new ArrayList<>(Arrays.asList(dateFormatSymbols.getShortWeekdays()));
        days.remove(Empty.STRING);
        String[] dayNames = new String[days.size()];
        days.toArray(dayNames);
        return isSevenDaysWeek ? dayNames : Arrays.copyOfRange(dayNames, 1, 6);
    }

    private void initHeadLine() {
        int headlineColor = getResources().getColor(R.color.headline);
        View line = new View(context);
        line.setId(getResources().getIdentifier(Id.HEADLINE, Id.id, packageName));
        final LayoutParams params = new LayoutParams(MATCH_PARENT, 2);
        int headerDaysId = getResources().getIdentifier(Id.LAYOUT_HEADER_DAYS, Id.id, packageName);
        params.addRule(RelativeLayout.BELOW, headerDaysId);
        line.setLayoutParams(params);
        line.setBackgroundColor(headlineColor);
        addView(line);
    }

    private void initGridView() {
        GridView gridView = new GridView(context);
        gridView.setNumColumns(isSevenDaysWeek ? 7 : 5);
        LayoutParams params = new LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        int headlineId = getResources().getIdentifier(Id.HEADLINE, Id.id, packageName);
        params.addRule(RelativeLayout.BELOW, headlineId);
        gridView.setLayoutParams(params);

        gridView.setOnTouchListener(this);
        gridView.setOnItemClickListener(this);

//        //TODO: remove the mock data
//        CalendarAdapter adapter = new CalendarAdapter();
        ArrayList<Integer> mockData = new ArrayList<>(30);
        for (int i = 0; i < 30; i++) {
            mockData.add(calendar.get(Calendar.MONTH) + 1);
        }
        mockAdapter = new ArrayAdapter<>(context, R.layout.mock_layout, mockData);


        gridView.setAdapter(mockAdapter);
        addView(gridView);
    }

    private static class CoordinatesValueHolder {
        static final float MIN_SCROLL_PATH = 20F;
        static float downY;
        static float upY;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (listener != null) {
            listener.onSelectedDateChanged(view);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        final float y = event.getY();
        final int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                CoordinatesValueHolder.downY = y;
                return false;
            case MotionEvent.ACTION_UP:
                CoordinatesValueHolder.upY = y;
                if (isScrollEvent(y)) {
                    updateCalendar(isForwardScroll());
                    return true;
                }
        }
        return false;
    }

    private boolean isScrollEvent(float upY) {
        float scrolledPath = Math.abs(upY - CoordinatesValueHolder.downY);
        return scrolledPath > CoordinatesValueHolder.MIN_SCROLL_PATH;
    }

    private boolean isForwardScroll() {
        return CoordinatesValueHolder.downY < CoordinatesValueHolder.upY;
    }

    private void updateCalendar(boolean isForwardScroll) {
        setNewMonthAndYear(isForwardScroll);

        //TODO: call adapter to handle changes
        //adapter.updateView(calendar);

        //TODO: remove the mock data
        Integer[] monthNumbers = new Integer[30];
        Arrays.fill(monthNumbers, calendar.get(Calendar.MONTH) + 1);
        ArrayList<Integer> mockData = new ArrayList<>(Arrays.asList(monthNumbers));
        mockAdapter.clear();
        mockAdapter.addAll(mockData);
        if (listener != null) {
            listener.onVisibleMonthChanged(calendar.get(Calendar.MONTH) - 1, calendar.get(Calendar.MONTH));
        }
    }

    void setNewMonthAndYear(boolean isForwardScroll) {
        calendar.roll(Calendar.MONTH, isForwardScroll);
        final int month = calendar.get(Calendar.MONTH);
        if (month == 0 && isForwardScroll) {
            calendar.roll(Calendar.YEAR, 1);
        } else if (month == 11 && !isForwardScroll) {
            calendar.roll(Calendar.YEAR, -1);
        }
        String newMonthAndYear = getMonthAndYear();
        monthYearTxtView.setText(newMonthAndYear);
    }
}
