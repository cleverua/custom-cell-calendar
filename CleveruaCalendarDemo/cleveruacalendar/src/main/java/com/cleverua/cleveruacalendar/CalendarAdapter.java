package com.cleverua.cleveruacalendar;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.MonthDisplayHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Artemiy Lysenko, on 9/22/15.
 */
public class CalendarAdapter extends BaseAdapter {

    private Context context;

    private LayoutInflater layoutInflater;
    private ColorsProvider colorsProvider;
    private CellOverlay cellOverlay;
    private Calendar calendar;

    public CalendarAdapter(@NonNull Context context, @Nullable CellOverlay cellOverlay, @Nullable ColorsProvider colorsProvider) {
        this.context = context;
        this.cellOverlay = cellOverlay;
        this.colorsProvider = colorsProvider;
        layoutInflater = LayoutInflater.from(context);
    }

    public void setColorsProvider(ColorsProvider colorsProvider) {
        this.colorsProvider = colorsProvider;
    }

    public void setCellOverlay(CellOverlay cellOverlay) {
        this.cellOverlay = cellOverlay;
    }

    void updateView(Calendar calendar) {
        this.calendar = calendar;

        //TODO: get month & year from the calendar; get number of days in the month; map days numbers to days names
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        return null;
    }
}


