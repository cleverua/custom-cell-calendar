package com.cleverua.cleveruacalendar;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

/**
 * @author Artemiy Lysenko, on 9/22/15.
 */
public class MockCellOverlayFactoryImpl implements CellOverlay {

    private Context context;

    public MockCellOverlayFactoryImpl(Context context) {
        this.context = context;
    }

    @Override
    public View setColorsArray(Integer[] colors) {
        TextView textView = new TextView(context);
        textView.setLayoutParams(new ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        if (colors.length > 0) {
            textView.setBackgroundColor(colors[0]);
        }
        return textView;
    }
}
