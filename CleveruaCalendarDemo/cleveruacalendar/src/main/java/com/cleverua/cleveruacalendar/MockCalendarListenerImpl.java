package com.cleverua.cleveruacalendar;

import android.graphics.Color;
import android.util.Log;
import android.view.View;

import java.util.Date;

/**
 * @author Artemiy Lysenko, on 9/25/15.
 */
public class MockCalendarListenerImpl implements CalendarListener {
    private static final String TAG = "MockCalendarListenerImpl";

    @Override
    public void onVisibleMonthChanged(int previousMonth, int newMonth) {
        Log.i(TAG, "Month was changed from " + previousMonth + " to " + newMonth);
    }

    @Override
    public void onSelectedDateChanged(Date selectedDate) {
        Log.i(TAG, "selectedDate: " + selectedDate);
    }

    @Override
    public void onSelectedDateChanged(View selectedCell) {
        selectedCell.setBackgroundColor(Color.BLUE);
    }
}
