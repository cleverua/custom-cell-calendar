package com.cleverua.cleveruacalendar;

import android.view.View;

/**
 * @author Artemiy Lysenko, on 9/22/15.
 */
public interface CellOverlay {

    View setColorsArray(Integer[] colors);
}
